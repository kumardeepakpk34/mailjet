const { requestMailJet } = require('../functions/mailjet')

module.exports = async (req, res) => {
    try {
        const results = await requestMailJet()
        if (results) {
            res.status(200).send({
                message: "success",
                results
            })
        }
    }
    catch (err) {
        res.status(406).send({
            message: err.message
        })
    }
}