const { oauthCreds } = require('../middleware/oauth')


module.exports = async (req, res) => {
    try {
        const transport = await oauthCreds()
        const mailOptions = {
            from: 'parihardpk024@gmail.com', // sender
            to: req.body.email, // receiver
            subject: 'My tutorial brought me here', // Subject
            html: '<p>You have received this email using nodemailer, you are welcome ;)</p>'// html body
        }

        transport.sendMail(mailOptions, function (err, result) {
            if (err) {
                res.send({
                    message: err
                })
            } else {
                transport.close();
                res.send({
                    message: 'Email has been sent: check your inbox!'
                })
            }
        })
    }
    catch (err) {
        res.status(406).send({
            message: err.message
        })
    }
}