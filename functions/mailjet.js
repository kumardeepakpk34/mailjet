const mailjet = require('node-mailjet')
    .connect('c4ed2a38ce8f9dc2b801f81df1a9e335', 'd7c3653352008d48ef8c91822b088dc6')

async function requestMailJet() {
    try {
        const request = mailjet
            .post("send", { 'version': 'v3.1' })
            .request({
                "Messages": [
                    {
                        "From": {
                            "Email": "becire6158@to200.com",
                            "Name": "Temp"
                        },
                        "To": [
                            {
                                "Email": "parihardpk024@gmail.com",
                                "Name": "Deepak"
                            }
                        ],
                        "Subject": "Greetings from Mailjet.",
                        "TextPart": "My first Mailjet email",
                        "HTMLPart": "<h3>Dear passenger 1, welcome to <a href='https://www.mailjet.com/'>Mailjet</a>!</h3><br />May the delivery force be with you!",
                        "CustomID": "AppGettingStartedTest",
                        "TemplateLanguage": true,
                        "Variables": {
                            "day": "Tuesday",
                            "personalmessage": "Happy birthday!"
                        }
                    }
                ]
            })
        request
            .then((result) => {
                console.log(result.body)
                return result.body
            })
            .catch((err) => {
                console.log(err.statusCode)
                throw err
            })
    } catch (err) {
        throw err
    }
}

module.exports = { requestMailJet }