
const sgMail = require('@sendgrid/mail');
require('dotenv').config();
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

module.exports = sgMail;

async function sendEmail(data) {

    const msg = {
        to: "email",
        from: "send grid registered email", // Use the email address or domain you verified above
        subject: 'Subject',
        html: `html email can be used or text as well`,
    };

    await sgMail.send(msg);
}