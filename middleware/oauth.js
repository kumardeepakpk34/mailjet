require('dotenv').config()
const nodemailer = require('nodemailer');
const { google } = require("googleapis");
const OAuth2 = google.auth.OAuth2;


async function oauthCreds() {
    try {
        const myOAuth2Client = new OAuth2(
            `${process.env.CLIENT_ID}`,
            `${process.env.CLIENT_SECRET_ID}`,
            "https://developers.google.com/oauthplayground"
        );
        myOAuth2Client.setCredentials({
            refresh_token: `${process.env.REFRESH_TOKEN}`
        });

        const myAccessToken = await myOAuth2Client.getAccessToken();


        const transport = await nodemailer.createTransport({
            service: "gmail",
            auth: {
                type: "OAuth2",
                user: "parihardpk024@gmail.com", //your gmail account you used to set the project up in google cloud console"
                clientId: `${process.env.CLIENT_ID}`,
                clientSecret: `${process.env.CLIENT_SECRET_ID}`,
                refreshToken: `${process.env.REFRESH_TOKEN}`,
                accessToken: myAccessToken //access token variable we defined earlier
            }
        });

        return transport;
    }
    catch (err) {
        throw err
    }
}

module.exports = { oauthCreds }

